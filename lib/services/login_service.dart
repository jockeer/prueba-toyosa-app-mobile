import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

class LoginService {

  Future login({required String email, required String password}) async {
    final url = Uri.parse('http://10.0.2.2:8085/api/auth/login');
    final parametros = {"email": email, "password":password};
    final resp = await http.post(url,
      body: jsonEncode(parametros),
      headers: {"Content-Type":'application/json'}
    );
    final decoded = await json.decode(resp.body);
    
    if (!decoded["ok"]) return 0;

    return decoded;

  }
  
}