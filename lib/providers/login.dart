import 'package:flutter/material.dart';

class LoginProvider extends ChangeNotifier{

  late String _email ='';
  late String _password = '';

  String get email {
    return _email;
  }

  set email(String email){
    _email=email;
    notifyListeners();
  }

  String get password {
    return _password;
  }
  set password(String password){
    _password=password;
    notifyListeners();
  }

}