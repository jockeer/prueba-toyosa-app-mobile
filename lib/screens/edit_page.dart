import 'package:flutter/material.dart';

class EditScreen extends StatelessWidget {
  const EditScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
   return Scaffold(
      appBar: AppBar(title: Text('Editar item'),),

      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Editar',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),),
              SizedBox(height: 20,),

             const Text('Nombre item',style: TextStyle(fontWeight: FontWeight.bold),),
              TextFormField(
                initialValue: 'ss',
                onChanged: (value){
                  //  provider.email = value;
                },
              ),
              SizedBox(height: 10,),

              const Text('Descripcion',style: TextStyle(fontWeight: FontWeight.bold),),
              TextFormField(
                onChanged: (value){
                  // provider.password = value;
                },
              ),

              Center(
                child: ElevatedButton(
                  onPressed: (){},
                  child: Text('Editar item'),
                ),
              ),

              SizedBox(height: 10,),
            ],
          ),
        ),
      ),
    );
  }
}