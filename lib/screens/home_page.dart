import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  final List items = [
    {
      "id":"1",
      "nombre":"Producto 1",
    },
    {
      "id":"2",
      "nombre":"Producto 2",
    },
    {
      "id":"3",
      "nombre":"Producto 3",
    },
    {
      "id":"4",
      "nombre":"Producto 4",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home'), centerTitle: true,),
      body: ListView(
        children: items.map((e){
          return ListTile(
            onTap: (){
              Navigator.pushNamed(context, 'edit', arguments: e);
            },
            title: Text(e["nombre"]),
            trailing: IconButton(
              onPressed: (){
                //TODO: servicio que elimine mi item del usuario
              },
              icon: Icon(Icons.delete_forever_rounded, color: Colors.red,),
            ),
          );
        }).toList(),
      ),
    floatingActionButton: FloatingActionButton(
      onPressed: (){
        Navigator.pushNamed(context, 'add');
      },
      child: Icon(Icons.add),
    ),
    );
  }
}