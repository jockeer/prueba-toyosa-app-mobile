import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pruebaappmovil/providers/login.dart';
import 'package:pruebaappmovil/services/login_service.dart';


class LoginScreen extends StatelessWidget {

  LoginScreen({Key? key}) : super(key: key);

  final loginService = LoginService();

  @override
  Widget build(BuildContext context) {

    final provider = Provider.of<LoginProvider>(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('email',style: TextStyle(fontWeight: FontWeight.bold),),
            TextFormField(
             
             onChanged: (value){
               provider.email = value;
             },
            ),
            SizedBox(height: 10,),
            const Text('password',style: TextStyle(fontWeight: FontWeight.bold),),
            
            TextFormField(
              onChanged: (value){
                provider.password = value;
              },
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () async {
                    final login = await loginService.login(email: provider.email, password: provider.password);
                    Navigator.pushNamed(context, 'home');
                  },
                  child: Text('Ingresar'),
                ),
                SizedBox(width: 10,),
                ElevatedButton(
                  onPressed: () async {
                    // final login = await loginService.login(email: provider.email, password: provider.password);
                    Navigator.pushNamed(context, 'register');
                  },
                  child: Text('Registrarse'),
                ),
              ]
            )
          ],
        ),
      ),
    );
  }
}