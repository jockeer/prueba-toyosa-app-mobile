import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pruebaappmovil/providers/login.dart';
import 'package:pruebaappmovil/routes/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: ( _ ) => LoginProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: 'login',
        routes: getRoutes(),
      ),
    );
  }
}


