
import 'package:flutter/material.dart';
import 'package:pruebaappmovil/screens/add_page.dart';
import 'package:pruebaappmovil/screens/edit_page.dart';
import 'package:pruebaappmovil/screens/home_page.dart';
import 'package:pruebaappmovil/screens/login_page.dart';
import 'package:pruebaappmovil/screens/register_page.dart';

Map<String, WidgetBuilder> getRoutes(){
  return <String, WidgetBuilder> {
    'login': (_) => LoginScreen(),
    'register': (_) => RegisterScreen(),
    'home': (_) => HomeScreen(),
    'edit': (_) => EditScreen(),
    'add': (_) => AddScreen()
  };
}